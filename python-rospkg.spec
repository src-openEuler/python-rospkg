%global srcname rospkg

Name:           python-%{srcname}
Version:        1.5.0
Release:        1%{?dist}
Summary:        Utilities for ROS package, stack, and distribution information

License:        BSD
URL:            http://ros.org/wiki/rospkg
Source0:        https://github.com/ros-infrastructure/%{srcname}/archive/%{version}/%{version}.tar.gz

BuildArch:      noarch

%description
The ROS packaging system simplifies development and distribution of code
libraries. It enables you to easily specify dependencies between code
libraries, easily interact with those libraries from the command-line, and
release your code for others to use.


%package doc
Summary:        Documentation for %{name}
BuildRequires:  make
BuildRequires:  python%{python3_pkgversion}-catkin-sphinx
BuildRequires:  python%{python3_pkgversion}-pytest
BuildRequires:  python%{python3_pkgversion}-sphinx

%description doc
HTML documentation for the '%{srcname}' Python module.


%package -n python%{python3_pkgversion}-%{srcname}
Summary:        %{summary}
BuildRequires:  python%{python3_pkgversion}-catkin_pkg
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-distro
BuildRequires:  python%{python3_pkgversion}-pytest
BuildRequires:  python%{python3_pkgversion}-PyYAML
BuildRequires:  python%{python3_pkgversion}-setuptools

%if %{undefined __pythondist_requires}
Requires:       python%{python3_pkgversion}-catkin_pkg
Requires:       python%{python3_pkgversion}-distro
Requires:       python%{python3_pkgversion}-PyYAML
%endif

%description -n python%{python3_pkgversion}-%{srcname}
The ROS packaging system simplifies development and distribution of code
libraries. It enables you to easily specify dependencies between code
libraries, easily interact with those libraries from the command-line, and
release your code for others to use.


%prep
%autosetup -p1 -n %{srcname}-%{version}

find test -type f | xargs sed -i '1{s@^#!/usr/bin/env python@#!%{__python3}@}'


%build
%py3_build

%make_build -C doc html man SPHINXBUILD=sphinx-build-%{python3_version}
rm doc/_build/html/.buildinfo


%install
%py3_install

# backwards compatibility symbolic links
pushd %{buildroot}%{_bindir}
for i in *; do
  ln -s ./$i python3-$i
done
popd
install -p -m0644 -D doc/man/rosversion.1 %{buildroot}%{_mandir}/man1/rosversion.1


%check

PYTHONPATH=%{buildroot}%{python3_sitelib} \
  %{__python3} -m pytest test


%files doc
%doc doc/_build/html

%files -n python%{python3_pkgversion}-%{srcname}
%doc README.md
%{python3_sitelib}/%{srcname}/
%{python3_sitelib}/%{srcname}-%{version}-py%{python3_version}.egg-info/
%{_bindir}/rosversion
%{_bindir}/python3-rosversion
%{_mandir}/man1/rosversion.1.*


%changelog
* Thu May 4 2023 will_niutao <niutao2@huawei.com> - 1.5.0-1
- Init for openEuler
